module "minikube" {
  source = "scholzj/minikube/aws"
  version = "1.16.4"

  aws_region          = "ap-south-1"
  cluster_name        = "my-minikube"
  aws_instance_type   = "t3.large"
  #place your public key path here
  ssh_public_key      = "C:\\k8-09\\techietrainersgitlab.pub"
  aws_subnet_id       = "subnet-0e24d60da79abe8b0"
  hosted_zone         = "techietrainers.com"
  hosted_zone_private = false
  ami_image_id        = ""

  tags = {
    Application = "Minikube"
  }
  addons = [
    "https://raw.githubusercontent.com/scholzj/terraform-aws-minikube/master/addons/storage-class.yaml",
    "https://raw.githubusercontent.com/scholzj/terraform-aws-minikube/master/addons/csi-driver.yaml",
    "https://raw.githubusercontent.com/scholzj/terraform-aws-minikube/master/addons/metrics-server.yaml",
    "https://raw.githubusercontent.com/scholzj/terraform-aws-minikube/master/addons/dashboard.yaml",
    "https://raw.githubusercontent.com/scholzj/terraform-aws-minikube/master/addons/external-dns.yaml",
  ]
}
