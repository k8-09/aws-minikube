### K8 Install in AWS through minikube
### Pre requisites

1. Terraform
2. AWS CLI
3. Kubectl
4. Domain registered as hosted zone in AWS

### Install terraform

Please refer the below video to install terraform in Windows.

```
https://www.youtube.com/watch?v=H3hBU-9I8So&list=PL1jY4BuFJn1dAIrHL299HW-tH26eq2qOz&index=3
```
### Install AWS CLI

Download the below and install like any other software in windows

```
https://awscli.amazonaws.com/AWSCLIV2.msi
```
### Install kubectl
Follow the instructions in the below link

```
https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/
```
### create IAM user and configure using aws
1. Create IAM user and add administrator policy.
2. Save Access key and Secret Key in your local. don't push to git repos.
3. configure AWS CLI with these credentials
```
aws configure
```
### purchase one domain and create hosted zone in route53

### terraform commands
Initialize the terraform
```
terraform init
```
Plan command to know the resources it is going to create. it will create the resources
```
terraform plan
```
Apply to create the resources
```
terraform apply
```

### finally destroy and unsubscribe centos7
```
terraform destroy
```
### Connect to server and get the kubeconfig
```
scp -i <your-key> centos@<IP>:~/kubeconfig ~/.kube/config
```