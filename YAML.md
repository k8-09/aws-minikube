### Markup Languages
All Markup languages are used to carry the information in the software world. System-1 uses one of the below notation to transfer data to another system.

YAML --> Yet Another Markup Language <br/>
HTML --> HyperText Markup Language <br/>
XML --> Extensible Markup Language <br/>
JSON --> JavaScript Object Notation<br/>

### Normal Template
```
NAME: 
ACCOUNT NO:
BRANCH: 
DATE:
MONEY:
Address

    Door No
    Street
    City
```
### XML
```
<Name>Siva</Name>
<DOB>08-08-08</DOB>
<Password>Siva123</Password>
<Address>
    <PermAddress>
        <DoorNo>123-123</DoorNo>
        <Street>Gandi Street</Street>
        <City>Hyd</City>
    </PermAddress>
    <TempAddress>
        <DoorNo>123-123</DoorNo>
        <Street>Gandi Street</Street>
        <City>Hyd</City>
    </TempAdress>
</Address>
```

### JSON
```
{
    "Name": "Siva",
    "DOB" : "08-08-08",
    "Password": "siva123"
    "Address": [
        {
        "DoorNo": "123-123"
        "Street": "Gandhi Street"
        "City": "Hyd"
        },
        {
        "DoorNo": "123-123"
        "Street": "Gandhi Street"
        "City": "Hyd"
        }
    ]
}
```

### YAML
```
Name: Siva
DOB: 08-08-08
Password: siva123
Address:
- PermAddress:
    DoorNo: 123-123
    Street: Gandhi Street
    City: Hyd
- TempAddress:
    DoorNo: 123-123
    Street: Gandhi Street
    City: Hyd
```